package college.shawinigan;

public abstract class Sort {
	protected int[] _values;
	
	public int[] getValues(){
		return _values;
	}
	
	public Sort(int[] values) {
		_values = values;
	}
	
	public abstract void sort();
}
