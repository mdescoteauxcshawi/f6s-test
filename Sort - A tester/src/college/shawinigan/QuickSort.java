package college.shawinigan;

public class QuickSort extends Sort {

	public QuickSort(int[] values) {
		super(null);
	}

	@Override
	public void sort() {
		quickSort(_values, 0, _values.length);
	}
	
	private void quickSort(int[] values, int lowIndex, int hiIndex) {
		if (values.length < 0)
			return;
 
		if (lowIndex >= hiIndex)
			return;
 
		int middle = lowIndex + (hiIndex - lowIndex) / 2;
		int pivot = values[middle];
 
		//gauche < pivot et droite > pivot
		int i = lowIndex, j = hiIndex;
		while (i <= j) {
			while (values[i] < pivot) {
				i++;
			}
 
			while (values[j] > pivot) {
				j--;
			}
 
			if (i <= j) {
				int temp = values[i];
				values[i] = values[j];
				values[j] = temp;
				i++;
				j--;
			} 
		}
 
		// quickSort recursif sur les 2 parties
		if (lowIndex < j)
			quickSort(values, lowIndex, j);
 
		if (hiIndex > i)
			quickSort(values, i, hiIndex);
	}
}
