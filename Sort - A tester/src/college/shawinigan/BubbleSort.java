package college.shawinigan;

public class BubbleSort extends Sort {

	public BubbleSort(int[] values) {
		super(values);
	}

	@Override
	public void sort() {
	    for (int i = 1; i <= _values.length; i++) {
	        for (int j = 1; j < (_values.length - i); j++) {

	            if (_values[j - 1] > _values[j]) {
	                int temp = _values[j - 1];
	                _values[j - 1] = _values[j];
	                _values[j] = temp;
	            }
	        }
	    }
	}

}
